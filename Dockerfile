FROM thelastpickle/cassandra-reaper:3.2.0

USER root
RUN apk update && apk upgrade --no-cache

USER reaper

ENV REAPER_REPAIR_MANAGER_SCHEDULING_INTERVAL_SECONDS=5